import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ListView,
  Config,
  TouchableHighlight,
  View
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../styles/layout.js';
import store from 'react-native-simple-store';

import { Actions } from 'react-native-router-flux';

class GuestScreen extends Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      visible:true,
      dataSource: ds.cloneWithRows([]),
    };
  }

  _pressRow(rowID) {
    Actions.editguest({
      account_id:this.props.account_id,
      event_id: this.props.event_id,
      contact_id: rowID
    });
  }

  _changeStatus (rowID) {
    var action = this.state.status == 'in' ? 'out' : 'in';
    this.setState({visible:true});
    store.get('profile_data').then((data)=> {
      if (data === null || data === undefined) {
          // redirect to home page
      } else {
        fetch("http://greetdesk.com/api/v1/lists/" + this.props.list_id + "/contacts/" + rowID + "/" + action + ".json",
        {method: "POST",
      headers: {
        'Authorization': 'Bearer ' + data,
      }}).then((response) => response.json())
          .then((responseData) => {
            this.props.increment_account_list();
            this.setState({visible:false, status: responseData['status'], status_name: (responseData['status'] == 'in' ? 'Checkout' : 'Checkin')})

      }).catch((error) => {
            //this.setState({visible:false})
            console.warn(error);
      });
      }
    })
  }
  componentWillReceiveProps() {

      this.setState({visible:true})
      store.get('profile_data').then((data)=> {
        if (data === null || data === undefined) {
            // redirect to home page
        } else {
          fetch("http://greetdesk.com/api/v1/lists/" + this.props.list_id + "/contacts/" + this.props.contact_id + ".json",
          {method: "GET",
        headers: {
          'Authorization': 'Bearer ' + data,
        }}).then((response) => response.json())
            .then((responseData) => {
              this.setState({visible:false, status: responseData['status'], status_name: (responseData['status'] == 'in' ? 'Checkout' : 'Checkin'), id:responseData['id'], name: responseData['name'], contact_id: responseData['contact_id'],email: responseData['email'], tags: responseData['tags'].join(', ')})

        }).catch((error) => {
              //this.setState({visible:false})
              console.warn(error);
        });
        }
      })
  }

  componentDidMount(  ) {
    store.get('profile_data').then((data)=> {
      if (data === null || data === undefined) {
          // redirect to home page
      } else {
        fetch("http://greetdesk.com/api/v1/lists/" + this.props.list_id + "/contacts/" + this.props.contact_id + ".json",
        {method: "GET",
      headers: {
        'Authorization': 'Bearer ' + data,
      }}).then((response) => response.json())
          .then((responseData) => {
            this.setState({visible:false, id:responseData['id'], status: responseData['status'], status_name: (responseData['status'] == 'in' ? 'Checkout' : 'Checkin'), name: responseData['name'], contact_id: responseData['contact_id'],email: responseData['email'], tags: responseData['tags'].join(', ')})

      }).catch((error) => {
            //this.setState({visible:false})
            console.warn(error);
      });
      }
    })


  }

  render() {
    return (
      <View style={styles.container}>
      <Spinner visible={this.state.visible}/>

      <TouchableHighlight style={styles.list_item} onPress={() => {
        this._changeStatus(this.state.contact_id);
      }}><Text>{this.state.status_name}</Text></TouchableHighlight>

        <View>
          <Text>Name: {this.state.name}</Text>
        </View>
        <View>
          <Text>Email: {this.state.email}</Text>
        </View>
        <View>
          <Text>Tags: {this.state.tags}</Text>
        </View>
        <View>

        <TouchableHighlight style={styles.list_item} onPress={() => {
          this._pressRow(this.state.contact_id);
        }}><Text>Edit</Text></TouchableHighlight>


        </View>
      </View>
    );
  }
}


export default GuestScreen;

//name, email, tags
