import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ListView,
  Config,
  TouchableHighlight,
  View
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../styles/layout.js';
import store from 'react-native-simple-store';

class EventScreen extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      visible:true,
      dataSource: ds.cloneWithRows([]),
    };
  }

  _pressRow(rowID) {
    Actions.guests({account_id:this.props.account_id,
    event_id: this.props.event_id,
    list_id: rowID});

    /*
    this.props.navigator.push({
      id: 'guests',
      account_id:this.props.account_id,
      event_id: this.props.event_id,
      list_id: rowID
    })*/
  }

  componentDidMount(  ) {

    store.get('profile_data').then((data)=> {
      if (data === null || data === undefined) {
          // redirect to home page
      } else {
        fetch("http://greetdesk.com/api/v1/events/" + this.props.event_id + ".json",
        {method: "GET",
      headers: {
        'Authorization': 'Bearer ' + data,
      }}).then((response) => response.json())
          .then((responseData) => {
            this.setState({visible:false, dataSource: this.state.dataSource.cloneWithRows(responseData['lists'])})

      }).catch((error) => {
            this.setState({visible:false})
            console.warn(error);
      });
      }
    })

  }

  render() {
    return (
      <View style={styles.container}>
      <Spinner visible={this.state.visible}/>

      <ListView
        dataSource={this.state.dataSource}

        renderRow={(rowData) => <TouchableHighlight  onPress={() => {
          this._pressRow(rowData.id);
        }}><Text style={styles.list_item}>{rowData.name}</Text></TouchableHighlight>}
      />
      </View>
    );
  }
}


export default EventScreen;
