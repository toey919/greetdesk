import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';

var t = require('tcomb-form-native');
import store from 'react-native-simple-store';
var Form = t.form.Form;
import { Actions } from 'react-native-router-flux';
// here we are: define your domain model
var Person = t.struct({
  login: t.String,              // a required string
  password: t.String,
  //surname: t.maybe(t.String),  // an optional string
  //age: t.Number,               // a required number
  rememberMe: t.Boolean        // a boolean
});

var options = {
  fields: {
password: {
  password: true,
  secureTextEntry: true
}
}

}; // optional rendering options (see documentation)

var styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});

const LoginScreen = React.createClass({


  onPress() {
    var value = this.refs["form"].getValue();
    if (value) {

      fetch('http://greetdesk.com//oauth/token', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          password:value['password'],
          username:value['login'],
          grant_type: "password",
          client_id: "K0mHMqdgO5g-0aAM",
          client_secret: "UQyio4TnZiLd6QzZ4QjyBBt9a7q-4I_8kanUO_UZx9Pr8rfp9MjMRmbcfaRMifRl",

        })
      }).then((response) => response.json())
      .then((responseData) => {
        console.warn(navigator)
        store
        .save('profile_data', responseData['access_token']).then(() => Actions.accounts({type:'reset'}));

      });
    }

    if (value) { // if validation fails, value will be null
      console.log(value); // value here is an instance of Person
    }

  },


  render() {
    return (
      <View style={styles.container}>

        <Form
          ref="form"
          type={Person}
          options={options}
        />
        <TouchableHighlight style={styles.button} onPress={this.onPress} underlayColor='#99d9f4'>
          <Text style={styles.buttonText}>Sign In</Text>
        </TouchableHighlight>
      </View>
    )
  }
});

export default LoginScreen;
