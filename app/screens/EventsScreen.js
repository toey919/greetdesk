import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ListView,
  Config,
  Button,
  TouchableHighlight,
  View
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../styles/layout.js';
import store from 'react-native-simple-store';
import { Actions } from 'react-native-router-flux';

class EventsScreen extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      visible:true,
      dataSource: ds.cloneWithRows([]),
    };
  }

  _showContacts(rowID) {
    Actions.contacts({account_id:this.props.account_id});
  }

  _pressRow(rowID) {
    Actions.event({account_id:this.props.account_id, event_id:rowID});
  }

  componentDidMount(  ) {
    this.props.set_account_id(this.props.account_id);

    store.get('profile_data').then((data)=> {
      if (data === null || data === undefined) {
          // redirect to home page
      } else {
        fetch("http://greetdesk.com/api/v1/accounts/" + this.props.account_id + "/events.json",
        {method: "GET",
      headers: {
        'Authorization': 'Bearer ' + data,
      }}).then((response) => response.json())
          .then((responseData) => {
            this.setState({visible:false, dataSource: this.state.dataSource.cloneWithRows(responseData)})

      }).catch((error) => {
            this.setState({visible:false})
            console.warn(error);
      });
      }
    })


  }

  render() {
    return (
      <View style={styles.container}>
      <Spinner visible={this.state.visible}/>
      <View style={{flex:1}}>

      <Button
  onPress={() => this._showContacts()}
  title="Contacts"
/>


      </View>
      <View style={{flex:3}}>
      <ListView

        dataSource={this.state.dataSource}

        renderRow={(rowData) => <TouchableHighlight  onPress={() => {
          this._pressRow(rowData.id);
        }}><Text style={styles.list_item}>{rowData.name}</Text></TouchableHighlight>}
      />
      </View>
      </View>
    );
  }
}


export default EventsScreen;
