import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  ListView,
  Config,
  TouchableHighlight,
  View
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../styles/layout.js';
import store from 'react-native-simple-store';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from "react-native-easy-grid";

import { Actions } from 'react-native-router-flux';

class GuestsScreen extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      attendees:'',
      checked:'',
      visible: true,
      dataSource: ds.cloneWithRows([]),
    };
  }

  _pressRow(rowID) {
    Actions.guest({account_id:this.props.account_id,
    event_id: this.props.event_id,
    contact_id: rowID,
    list_id: this.props.list_id});
  }

  componentDidUpdate(prevProps, prevState) {

    if (prevProps.account_list != this.props.account_list) {
      this.setState({input_text:''})
      store.get('profile_data').then((data)=> {
        if (data === null || data === undefined) {
            // redirect to home page
        } else {
          fetch("http://greetdesk.com/api/v1/lists/" + this.props.list_id + "/contacts.json",
          {method: "GET",
        headers: {
          'Authorization': 'Bearer ' + data,
        }}).then((response) => response.json())
            .then((responseData) => {
              function isChecked(person) {
                return person.status == 'in';
              }
              this.setState({visible:false, checked:responseData.filter(isChecked).length, attendees: responseData.length, data_array: responseData, dataSource: this.state.dataSource.cloneWithRows(responseData)})

        }).catch((error) => {
              //this.setState({visible:false})
              console.warn(error);
        });
        }
      })
    }
  }

  componentDidMount(  ) {

    store.get('profile_data').then((data)=> {
      if (data === null || data === undefined) {
          // redirect to home page
      } else {
        fetch("http://greetdesk.com/api/v1/lists/" + this.props.list_id + "/contacts.json",
        {method: "GET",
      headers: {
        'Authorization': 'Bearer ' + data,
      }}).then((response) => response.json())
          .then((responseData) => {
            function isChecked(person) {
              return person.status == 'in';
            }
            this.setState({visible:false, checked:responseData.filter(isChecked).length, attendees: responseData.length, data_array: responseData, dataSource: this.state.dataSource.cloneWithRows(responseData)})

      }).catch((error) => {
            //this.setState({visible:false})
            console.warn(error);
      });
      }
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner visible={this.state.visible}/>
      <Grid>
    <Row size={15}>
    <Text>Event Name</Text>
    <Text>Attendees: {this.state.attendees}</Text>
    <Text>Checked: {this.state.checked}</Text></Row>
    <Row size={85}>


      <ListView
        dataSource={this.state.dataSource}
        renderHeader={() => <View style={styles.search_container}>
            <TextInput
              style={styles.input}
              placeholder="Search..."
              value={this.state.input_text}
              onChangeText={(text) =>{
                  this.setState({input_text:text})
                  var data_array = this.state.data_array;
                  var rows = [];

                  for (var i=0; i < data_array.length; i++) {
                     var stateName = data_array[i]['name'].toLowerCase();

                     if(stateName.search(text.toLowerCase()) !== -1){
                       rows.push({'id': data_array[i]['id'], 'name':data_array[i]['name'], 'status':data_array[i]['status']});
                     }

                   }

                   this.setState({dataSource:this.state.dataSource.cloneWithRows(rows)});
                }}
            />
          </View>
        }
        renderRow={(rowData) => <TouchableHighlight onPress={() => {
          this._pressRow(rowData.contact_id);
        }}><Text style={styles.list_item}>{rowData.name} {rowData.status == 'in' ? <Icon name="check" size={30}  /> : ''}</Text></TouchableHighlight>}
      />

      </Row>
  </Grid>
      </View>
    );
  }
}


export default GuestsScreen;
