/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import {
  AppRegistry,
  Dimensions,
  TouchableHighlight,
  StyleSheet,
  Alert,
  Text,
  AlertIOS,
  View
} from 'react-native';
import Camera from 'react-native-camera';

import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../styles/layout.js';
import store from 'react-native-simple-store';

import { Actions } from 'react-native-router-flux';

import Icon from 'react-native-vector-icons/FontAwesome';
//const myIcon = (<Icon name="rocket" size={30} color="#900" />)

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  goBack() {
    Actions.pop();
  }

  _onBarCodeRead(e) {
    Alert.alert(
        "Barcode Found!",
        "Type: " + e.type + "\nData: " + e.data
    );

  }


  render() {
    return (
      <View style={styles.container_empty}>
      <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
          onBarCodeRead={(e) =>{
            this._onBarCodeRead(e);
          }}
          style={styles.preview}
          aspect={Camera.constants.Aspect.fill}>
          <Text style={styles.capture} onPress={this.goBack.bind(this)}>[Cancel]</Text>
        </Camera>

      </View>
    );
  }
}
