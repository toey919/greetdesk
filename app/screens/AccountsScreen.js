import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ListView,
  Config,
  TouchableHighlight,
  View
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../styles/layout.js';
import store from 'react-native-simple-store';
import { Actions } from 'react-native-router-flux';


class AccountsScreen extends Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      visible:true,
      dataSource: ds.cloneWithRows([]),
    };
  }

  _pressRow(rowID) {
    Actions.events({account_id:rowID});
    /*
    this.props.navigator.push({
      id: 'events',
      account_id:rowID
    })*/
  }

  componentDidMount(  ) {

    store.get('profile_data').then((data)=> {
      if (data === null || data === undefined) {
          // redirect to home page
      } else {
        fetch("http://greetdesk.com/api/v1/accounts.json",
        {method: "GET",
      headers: {
        'Authorization': 'Bearer ' + data,
      }}).then((response) => response.json())
          .then((responseData) => {
            this.setState({visible:false, dataSource: this.state.dataSource.cloneWithRows(responseData)})

      }).catch((error) => {
            //this.setState({visible:false})
            console.warn(error);
      });
      }
    })


  }

  render() {
    return (
      <View style={styles.container}>
      <Spinner visible={this.state.visible}/>
      <ListView
        dataSource={this.state.dataSource}

        renderRow={(rowData) => <TouchableHighlight onPress={() => {
          this._pressRow(rowData.id);
        }}><Text style={styles.list_item}>{rowData.name} </Text></TouchableHighlight>}
      />
      </View>
    );
  }
}


export default AccountsScreen;
