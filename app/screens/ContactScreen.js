import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ListView,
  Config,
  TouchableHighlight,
  View
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../styles/layout.js';
import store from 'react-native-simple-store';

import { Actions } from 'react-native-router-flux';

class ContactScreen extends Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      visible:true,
      dataSource: ds.cloneWithRows([]),
    };
  }

  _pressRow(rowID) {
    Actions.editcontact({account_id:this.props.account_id,
    event_id: this.props.event_id,
    contact_id: rowID});
  }

  componentWillReceiveProps() {

      this.setState({visible:true})
      store.get('profile_data').then((data)=> {
        if (data === null || data === undefined) {
            // redirect to home page
        } else {
          fetch("http://greetdesk.com/api/v1/contacts/" + this.props.contact_id + ".json",
          {method: "GET",
        headers: {
          'Authorization': 'Bearer ' + data,
        }}).then((response) => response.json())
            .then((responseData) => {
              this.setState({visible:false, id:responseData['id'], name: responseData['name'], email: responseData['email'], tags: responseData['tags'].join(', ')})

        }).catch((error) => {
              //this.setState({visible:false})
              console.warn(error);
        });
        }
      })
  }

  componentDidMount(  ) {

    store.get('profile_data').then((data)=> {
      if (data === null || data === undefined) {
          // redirect to home page
      } else {
        fetch("http://greetdesk.com/api/v1/contacts/" + this.props.contact_id + ".json",
        {method: "GET",
      headers: {
        'Authorization': 'Bearer ' + data,
      }}).then((response) => response.json())
          .then((responseData) => {
            this.setState({visible:false, id:responseData['id'], name: responseData['name'], email: responseData['email'], tags: responseData['tags'].join(', ')})

      }).catch((error) => {
            //this.setState({visible:false})
            console.warn(error);
      });
      }
    })


  }

  render() {
    return (
      <View style={styles.container}>
      <Spinner visible={this.state.visible}/>
        <View>
          <Text>Name: {this.state.name}</Text>
        </View>
        <View>
          <Text>Email: {this.state.email}</Text>
        </View>
        <View>
          <Text>Tags: {this.state.tags}</Text>
        </View>
        <View>
        <TouchableHighlight style={styles.list_item} onPress={() => {
          this._pressRow(this.state.id);
        }}><Text>Edit</Text></TouchableHighlight>
        </View>
      </View>
    );
  }
}


export default ContactScreen;

//name, email, tags
