import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  ListView,
  Config,
  TouchableHighlight,
  View
} from 'react-native';

import ContactsHeader from '../components/ContactsHeader.js';

import Spinner from 'react-native-loading-spinner-overlay';
import styles from '../styles/layout.js';
import store from 'react-native-simple-store';

import { Actions } from 'react-native-router-flux';

class ContactsScreen extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      visible: true,
      input_text:'',
      dataSource: ds.cloneWithRows([]),
      data_array:[]
    };
  }

  _pressRow(rowID) {
    Actions.contact({account_id:this.props.account_id,
    event_id: this.props.event_id,
    contact_id: rowID});
  }

  componentDidUpdate(prevProps, prevState) {

    if (prevProps.account_list != this.props.account_list) {
      this.setState({input_text:''})
      store.get('profile_data').then((data)=> {
        if (data === null || data === undefined) {
            // redirect to home page
        } else {
          fetch("http://greetdesk.com/api/v1/accounts/" + this.props.account_id + "/contacts.json",
          {method: "GET",
        headers: {
          'Authorization': 'Bearer ' + data,
        }}).then((response) => response.json())
            .then((responseData) => {
              this.setState({visible:false, data_array: responseData, dataSource: this.state.dataSource.cloneWithRows(responseData)})

        }).catch((error) => {
              this.setState({visible:false})
              console.warn(error);
        });
        }
      })
    }
  }

  componentDidMount(  ) {

    store.get('profile_data').then((data)=> {
      if (data === null || data === undefined) {
          // redirect to home page
      } else {
        fetch("http://greetdesk.com/api/v1/accounts/" + this.props.account_id + "/contacts.json",
        {method: "GET",
      headers: {
        'Authorization': 'Bearer ' + data,
      }}).then((response) => response.json())
          .then((responseData) => {
            this.setState({visible:false, data_array: responseData, dataSource: this.state.dataSource.cloneWithRows(responseData)})

      }).catch((error) => {
            this.setState({visible:false})
            console.warn(error);
      });
      }
    })


  }

  render() {
    return (
      <View style={styles.container}>
      <Spinner visible={this.state.visible}/>
      <ListView
        dataSource={this.state.dataSource}
        renderHeader={() => <View style={styles.search_container}>
            <TextInput
              style={styles.input}
              placeholder="Search..."
              value={this.state.input_text}
              onChangeText={(text) =>{
                  this.setState({input_text:text})
                  var data_array = this.state.data_array;
                  var rows = [];

                  for (var i=0; i < data_array.length; i++) {
                     var stateName = data_array[i]['name'].toLowerCase();

                     if(stateName.search(text.toLowerCase()) !== -1){
                       rows.push({'id': data_array[i]['id'], 'name':data_array[i]['name']});
                     }

                   }

                   this.setState({dataSource:this.state.dataSource.cloneWithRows(rows)});
                }}
            />
          </View>
        }
        renderRow={(rowData) => <TouchableHighlight onPress={() => {
          this._pressRow(rowData.id);
        }}><Text style={styles.list_item}>{rowData.name}</Text></TouchableHighlight>}
      />
      </View>
    );
  }
}


export default ContactsScreen;
