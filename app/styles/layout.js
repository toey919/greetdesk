import { Dimensions, Platform, StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  },
  search_container: {
    flex: 1,
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#C1C1C1',
  },
  input: {
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
  },
  red: {
    color: 'red'
  },
  container: {
    flex: 1,
    marginTop:(Platform.OS === 'ios') ? 64 : 54,
    backgroundColor: '#6FB8AF',
  },
  container_without_header: {
    flex: 1,
    paddingTop:(Platform.OS === 'ios') ? 64 : 54,
    backgroundColor: '#6FB8AF',
  },
  container_empty: {
    flex: 1,
  },
  list_item: {
    paddingLeft:5,
    paddingTop:10,
    fontSize: 20,
    paddingBottom:10
  },
  contacts_button: {
    borderWidth:2,
    borderColor:'white',
    borderRadius:10,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
  }
});
